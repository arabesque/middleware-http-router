import {createRoute} from "../src/lib/route";
import {Context, createListener} from "@arabesque/listener-http";
import {createRouter} from "../src/lib/router";
import {createApplication} from "@arabesque/core";
import {createServer, IncomingMessage, ServerResponse} from "http";

class CustomResponse<Request extends IncomingMessage = IncomingMessage> extends ServerResponse<Request> {
    public body: string | undefined;

    end(cb: () => void) {
        return super.end(this.body, cb);
    }
}

const router = createRouter(createRoute<Context<IncomingMessage, CustomResponse>, {
    bar: string
}>("GET", "/foo/:bar", (context, {bar}) => {
    context.response.statusMessage = bar;
    context.response.body = bar;

    return Promise.resolve();
}));

const server = createServer({
    ServerResponse: CustomResponse
});

const listener = createListener<IncomingMessage, CustomResponse>(server);
const application = createApplication(listener, router);

application(3000).then(() => {
    console.log('STARTED');
})
